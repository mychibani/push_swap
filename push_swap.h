/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmarchai <lmarchai@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/19 05:15:46 by lmarchai          #+#    #+#             */
/*   Updated: 2023/01/28 20:57:13 by lmarchai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libft/libft.h"
# include <stdlib.h>

typedef struct s_list_int
{
	int					content;
	int					index;
	struct s_list_int	*next;
}	t_list_int;

int		main(int argc, char **argv);
void	sa(t_list_int **a);
void	sb(t_list_int **b);
void	ss(t_list_int **a, t_list_int **b);
void	pa(t_list_int **a, t_list_int **b);
void	pb(t_list_int **a, t_list_int **b);
void	ra(t_list_int **a);
void	rb(t_list_int **b);
void	rr(t_list_int **a, t_list_int **b);
void	rra(t_list_int **a);
void	rrb(t_list_int **b);
void	print_list(t_list_int *a, t_list_int *b);

#endif