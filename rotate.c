/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmarchai <lmarchai@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/19 06:00:20 by lmarchai          #+#    #+#             */
/*   Updated: 2023/01/27 22:23:17 by lmarchai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <stdio.h>

void	ra(t_list_int **a)
{
	t_list_int	*first;
	t_list_int	*list;

	printf("ra\n");
	first = *a;
	list = (*a)->next;
	*a = (*a)->next;
	first->next = NULL;
	while (list->next != NULL)
		list = list->next;
	list->next = first;
}

void	rb(t_list_int **b)
{
	t_list_int	*first;
	t_list_int	*list;

	printf("rb\n");
	first = *b;
	list = (*b)->next;
	*b = (*b)->next;
	first->next = NULL;
	while (list->next != NULL)
		list = list->next;
	list->next = first;
}

void	rr(t_list_int **a, t_list_int **b)
{
	ra(a);
	rb(b);
}
