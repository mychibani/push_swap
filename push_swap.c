/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmarchai <lmarchai@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/19 05:15:43 by lmarchai          #+#    #+#             */
/*   Updated: 2023/01/29 01:43:55 by lmarchai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <stdio.h>


int	ft_atoi_push_swap(const char *str)
{
	int			i;
	long int	nbr;
	int			sign;

	i = 0;
	nbr = 0;
	sign = 1;
	while (str[i] == '\f' || str[i] == '\n' || str[i] == '\r'
		|| str[i] == '\t' || str[i] == '\v' || str[i] == ' ')
		i++;
	if (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			sign = -sign;
		i++;
	}
	while (str[i] <= '9' && str[i] >= '0')
	{
		nbr = nbr * 10 + str[i] - 48;
		i++;
	}
	if (nbr * sign > 2147483647)
		return (0);
	return (nbr * sign);
}

int	ft_lstsize_int(t_list_int *lst)
{
	int			i;
	t_list_int	*actual;

	i = 0;
	actual = lst;
	while (actual)
	{
		actual = actual->next;
		i++;
	}
	return (i);
}

t_list_int	*ft_lstnew_int(int content)
{
	t_list_int	*l;

	l = malloc(sizeof(t_list_int));
	if (!l)
		return (NULL);
	l->index = 0;
	l->content = content;
	l->next = NULL;
	return (l);
}

t_list_int	*ft_lstlast_int(t_list_int *lst)
{
	t_list_int	*actual;

	if (lst == 0)
		return (lst);
	actual = lst;
	while (actual->next)
		actual = actual->next;
	return (actual);
}

void	ft_lstadd_back_int(t_list_int **lst, t_list_int *new)
{
	t_list_int	*end;

	if (lst)
	{
		if (*lst)
		{
			end = ft_lstlast_int(*lst);
			end->next = new;
		}
		else
			*lst = new;
	}
}

void	print_list(t_list_int *a, t_list_int *b)
{
	t_list_int *firsta = a;
	t_list_int *firstb = b;

	printf("values\n------------------\n");
	while (a != NULL || b != NULL)
	{
		if (a == NULL)
			printf("    ");
		else
			printf("%d  ", a->content);
		if (b != NULL)
			printf("%d", b->content);
		printf("\n");
		if (a != NULL && a->next != NULL)
		a = a->next;
		else
			a = NULL;
		if (b != NULL && b->next != NULL)
			b = b->next;
		else
			b = NULL;
	}
	if (a == NULL)
		printf("    ");
	else
		printf("%d  ", a->content);
	if (b != NULL)
		printf("%d", b->content);
	a = firsta;
	b = firstb;
	printf("\n-   - \na   b\n");
	printf("------------------\n");
}

void	print_index(t_list_int *a, t_list_int *b)
{
	t_list_int	*firsta = a;
	t_list_int	*firstb = b;

	printf("index\n------------------\n");
	while (a != NULL || b != NULL)
	{
		if (a == NULL)
			printf("    ");
		else
			printf("%d  ", a->index);
		if (b != NULL)
			printf("%d", b->index);
		printf("\n");
		if (a != NULL && a->next != NULL)
		a = a->next;
		else
			a = NULL;
		if (b != NULL && b->next != NULL)
			b = b->next;
		else
			b = NULL;
	}
	if (a == NULL)
		printf("    ");
	else
		printf("%d  ", a->index);
	if (b != NULL)
		printf("%d", b->index);
	a = firsta;
	b = firstb;
	printf("\n-   - \na   b\n");
	printf("------------------\n");
}

t_list_int	*get_arg(char **argv)
{
	int			i;
	t_list_int	*a;
	t_list_int	*new;

	i = 1;
	a = malloc(sizeof(t_list_int));
	if (!a)
		return (a);
	a = ft_lstnew_int(ft_atoi(argv[i]));
	while (argv[++i])
	{
		new = ft_lstnew_int(ft_atoi(argv[i]));
		if (new == NULL)
			return (a);
		ft_lstadd_back_int(&a, new);
	}
	return (a);
}

t_list_int	*get_unindex_val(t_list_int *l)
{
	t_list_int	*first;

	while (l->next != NULL)
	{
		if (l->index == 0)
			return (l);
		l = l->next;
	}
	return (l);
}

void	define_index(t_list_int *l)
{
	t_list_int	*first;
	t_list_int	*last_value;
	int			nbr_nodes;
	int			index;

	index = 1;
	nbr_nodes = ft_lstsize_int(l);
	first = l;
	while (index <= nbr_nodes)
	{
		last_value = get_unindex_val(l);
		while (l->next != NULL)
		{
			if (last_value->content > l->content && l->index == 0)
			{
				last_value = l;
				l = first;
			}
			l = l->next;
		}
		if (last_value->content > l->content && l->index == 0)
		{
			last_value = l;
			l = first;
		}
		l = last_value;
		l->index = index;
		index++;
		l = first;
	}
}

int	check_sorted(t_list_int *l)
{
	t_list_int	*first;

	first = l;
	while (l->next != NULL)
	{
		if (l->content < l->next->content)
			l = l->next;
		else
		{
			l = first;
			return (0);
		}
	}
	l = first;
	return (1);
}

int	check_errors(char **l)
{
	int	i;
	int	j;

	i = 1;
	while (l[i])
	{
		j = 0;
		while (l[i][j])
		{
			if (ft_isdigit(l[i][j]) == 0 && l[i][j] && l[i][j] != '-'
				&& l[i][j] != '\f' && l[i][j] != '\n' && l[i][j] != '\r'
				&& l[i][j] != '\t' && l[i][j] != '\v' && l[i][j] != ' ')
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

void	send_to_b(t_list_int **a, t_list_int **b, t_list_int node, int i)
{
	int	total;

	total = ft_lstsize_int(*a);
	if (i >= total / 2)
		while ((*a)->index != node.index)
			rra(a);
	else
		while ((*a)->index != node.index)
			ra(a);
	pb(a, b);
}

t_list_int get_second(t_list_int **a, t_list_int **b,
	t_list_int node, int median)
{
	t_list_int	*list;
	t_list_int	ret;

	if (!*a)
		return (**a);
	list = *a;
	ret.next = NULL;
	ret.content = 0;
	ret.index = 0;
	while (list->next != NULL)
	{
		if (list->index <= median && list->index != node.index)
			ret = *list;
		list = list->next;
	}
	if (list->index <= median && list->index != node.index)
		return (ret);
	return (ret);
}

t_list_int	get_first(t_list_int **a, t_list_int **b, int median)
{
	t_list_int	*list;
	t_list_int	ret;

	list = *a;
	while (list->next != NULL)
	{
		if (list->index <= median)
			return (*list);
		else
			list = list->next;
	}
	if (list->index <= median)
		return (*list);
	ret.next = NULL;
	ret.content = 0;
	ret.index = 0;
	return (ret);
}

void	send_to_a(t_list_int **a, t_list_int **b, t_list_int node, int i)
{
	int	total;

	total = ft_lstsize_int(*b);
	if (i > total / 2)
		while ((*b)->index != node.index)
			rrb(b);
	else
		while ((*b)->index != node.index)
			rb(b);
	pa(a, b);
}

void	insert_sort(t_list_int **a, t_list_int **b, int to_find)
{
	t_list_int	*list;
	int			i;

	i = 0;
	list = *b;
	while (list->next != NULL)
	{
		i++;
		if (list->index == to_find)
		{
			send_to_a(a, b, *list, i);
			list = *b;
			return ;
		}
		else
			list = list->next;
	}
	if (list->index == to_find)
		send_to_a(a, b, *list, i);
}

void	send_back(t_list_int **a, t_list_int **b, int median, int total)
{
	int nbr;

	while (total > 0)
	{
		insert_sort(a, b, total);
		total--;
	}
}

int	get_pos(t_list_int **a, t_list_int to_find)
{
	int i;
	t_list_int *list;

	i = 0;
	list = *a;
	while (list->next != NULL)
	{
		if (list->index == to_find.index)
			return (i);
		list = list->next;
		i++;
	}
	return (i);
}

t_list_int comp_double(t_list_int **a, t_list_int **b,
	t_list_int first, t_list_int second)
{
	int i;
	int j;
	int total;
	t_list_int *list;

	list = *a;
	i = 0;
	j = 0;
	total = ft_lstsize_int(*a);
	i = get_pos(a, first);
	j = get_pos(a, second);
	if (i >= total / 2)
		i = total - i;
	if (j >= total / 2)
		j = total - j;
	if (i < j)
		send_to_b(a, b, first, get_pos(a, first));
	else
		send_to_b(a, b, second, get_pos(a, second));
}


void	fast_sort(t_list_int **a, t_list_int **b, int total)
{
	int			j;
	int			median;
	int			nbr;
	t_list_int	temp;
	t_list_int	temp2;

	if (total >= 400)
		median = total / 11;
	else
		median = total / 5;
	nbr = median;
	j = 0;
	while (nbr < total)
	{
		temp = get_first(a, b, nbr);
		while (temp.index != 0)
		{
			temp2 = get_second(a, b, temp, nbr);
			if (temp2.index == 0)
				send_to_b(a, b, temp, get_pos(a, temp));
			else
				comp_double(a, b, temp, temp2);
			temp = get_first(a, b, nbr);
			j++;
		}
		nbr += median;
	}
	while (j < total - 3)
	{
		if ((*a)->index == total)
			ra(a);
		temp = get_first(a, b, j + 1);
		send_to_b(a, b, temp, get_pos(a, temp));
		j++;
	}
	send_back(a, b, median, total);
}


void	sort_3(t_list_int **a)
{
	if ((*a)->index == 1 && (*a)->index == 2)
	{
		sa(a);
		ra(a);
		return ;
	}
	if ((*a)->index == 2)
	{
		if ((*a)->next->index == 1)
			sa(a);
		else
			rra(a);
	}
	if ((*a)->index == 3)
	{
		if ((*a)->next->index == 2)
		{
			sa(a);
			rra(a);
		}
		else
			ra(a);
	}
}

void	sort_5(t_list_int **a, t_list_int **b, int total)
{
	t_list_int *list;

	list = *a;
	while (list->next != NULL)
	{
		if (list->index == 4 || list->index == 5)
		{
			send_to_b(a, b, *list, get_pos(a, *list));
			list = *a;
		}
		else
			list = list->next;
	}
	if (list->index == 4 || list->index == 5)
		send_to_b(a, b, *list, get_pos(a, *list));
	sort_3(a);
	if ((*b)->index == 4 && total == 5)
		rb(b);
	if (total == 5)
		pa(a, b);
	pa(a, b);
	if (total == 5)
		ra(a);
	ra(a);
}

void	choose_sort(t_list_int **a, t_list_int **b)
{
	int	total;

	total = ft_lstsize_int(*a);
	if (total == 2)
		ra(a);
	if (total == 3)
	{
		sort_3(a);
		return ;
	}
	if (total == 4 || total == 5)
		sort_5(a, b, total);
	if (total > 5)
		fast_sort(a, b, total);
}

int	main(int argc, char **argv)
{
	t_list_int	*a;
	t_list_int	*b;

	if (argc != 1 && check_errors(argv) == 0)
	{
		a = get_arg(argv);
		if (check_sorted(a) == 1)
			return (1);
		b = NULL;
		define_index(a);
		// print_list(a, b);
		// print_index(a, b);
		choose_sort(&a, &b);
		//print_list(a, b);
		//print_index(a, b);
	}
	return (0);
}
